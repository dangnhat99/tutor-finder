package com.example.tutorfinder.feature.resigter.chooseresigter

import android.widget.Toast
import com.example.tutorfinder.R
import com.example.tutorfinder.base.BaseFragment
import com.example.tutorfinder.base.viewBinding
import com.example.tutorfinder.databinding.BeginFragmentBinding

class ChooseRegisterFragment : BaseFragment(R.layout.begin_fragment) {

    private val binding by viewBinding(BeginFragmentBinding::bind)


    override fun initView() {
        binding.BeginTitle.text = getString(R.string.begin_title)
        binding.txtFirst.text = getString(R.string.phu_huynh)
        binding.txtSecond.text = getString(R.string.gia_su_text)
    }

    override fun initListener() {
        binding.btnFirst.setOnClickListener{
            Toast.makeText(context,"1",Toast.LENGTH_LONG).show()
        }
        binding.btnSecond.setOnClickListener{
            Toast.makeText(context,"2",Toast.LENGTH_LONG).show()
        }
    }


}