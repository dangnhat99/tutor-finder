package com.example.tutorfinder.feature.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewbinding.ViewBinding
import com.example.tutorfinder.R
import com.example.tutorfinder.base.BaseActivity
import com.example.tutorfinder.databinding.ActivityMainBinding

class MainActivity : BaseActivity() {
    private val binding by viewBinding (ActivityMainBinding::inflate)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }


    override fun initView() {
        TODO("Not yet implemented")
    }

    override fun initListener() {
        TODO("Not yet implemented")
    }
}