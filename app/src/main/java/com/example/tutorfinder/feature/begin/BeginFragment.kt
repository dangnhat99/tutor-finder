package com.example.tutorfinder.feature.begin

import android.view.View
import com.example.tutorfinder.R
import com.example.tutorfinder.base.BaseFragment
import com.example.tutorfinder.base.viewBinding
import com.example.tutorfinder.databinding.BeginFragmentBinding
import kotlinx.android.synthetic.main.begin_fragment.*

class BeginFragment:BaseFragment(R.layout.begin_fragment) {
    private val binding by viewBinding(BeginFragmentBinding::bind)

    override fun initView() {}

    override fun initListener() {
        binding.btnFirst.setOnClickListener{
            navController.navigate(R.id.BeginFrag_to_LoginAct)
        }

        binding.btnSecond.setOnClickListener{
            navController.navigate(R.id.BeginFragment_to_BeginRegisterFragment)
        }
    }
}