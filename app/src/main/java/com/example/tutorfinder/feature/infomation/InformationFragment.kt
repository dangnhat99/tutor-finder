package com.example.tutorfinder.feature.infomation

import com.example.tutorfinder.R
import com.example.tutorfinder.base.BaseFragment
import com.example.tutorfinder.base.viewBinding
import com.example.tutorfinder.databinding.BeginFragmentBinding
import com.example.tutorfinder.databinding.InformationFragmentBinding

class InformationFragment: BaseFragment(R.layout.information_fragment) {
    private val binding by viewBinding(InformationFragmentBinding::bind)
    override fun initView() {
        TODO("Not yet implemented")
    }

    override fun initListener() {
        TODO("Not yet implemented")
    }
}